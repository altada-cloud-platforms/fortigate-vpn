
output "FGTPublicIP" {
  value = aws_eip.FGTPublicIP.public_ip
}

output "Username" {
  value = "admin"
}

output "Password" {
  value = aws_instance.fgtvm.id
}

output "vpc_id" {
  value = aws_vpc.fgtvm-vpc.id
}

output "private_subnet" {
  value = aws_subnet.privatesubnetaz1.id
}

output "public_subnet" {
  value = aws_subnet.publicsubnetaz1.id
}
